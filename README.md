# Combining-Vertex-Information-into-a-VertexBuffer
VertexBuffer verbuf = mVertexBuffer();
verbuf.setPositions(vertexArray, 1.0f, null);
verbuf.setNormals(normalsArray);

void setPosition(VertexArray position, float scale, float [] bias);
void setNormals(VertexArray normals);
